package io.lakesidejar.maison.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.lakesidejar.maison.mapper.ItemMapper;
import io.lakesidejar.maison.pojo.Item;
import io.lakesidejar.maison.pojo.ItemExample;
import io.lakesidejar.maison.pojo.ItemExample.Criteria;

@Service
public class ItemService {

    @Autowired
    private ItemMapper itemMapper;

    public Item getItemById(long itemId) {
        //添加查询条件
        ItemExample example = new ItemExample();
        Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(itemId);
        List<Item> list = itemMapper.selectByExample(example);
        if (list != null && list.size() > 0) {
            Item item = list.get(0);
            return item;
        }
        return null;
    }

}
