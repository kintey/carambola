package io.lakesidejar.maison.service;

import io.lakesidejar.masion.pojo.Item;

public interface ItemService {

    Item getItemById(long itemId);
}
